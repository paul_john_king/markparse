<!--
The script `project` generates this file from the project sources, and
overwrites any changes made to it.  In order to permanently change this file,
edit the project sources and call the command `project document`.
-->

markparse - A command-line parser that outputs help in a Markdown format

The Python package `markparse` has subclasses of the classes `ArgumentParser`
and `Action` of the Python package `argparse` that output help and usage in
GitLab Flavored Markdown format.

*   Version: None
*   URI:     https://gitlab.com/paul_john_king/markparse
*   Author:  Paul John King <paul_john_king@web.de>
*   License: LGPLv3
*   Python:  >=2.7
*   Classifiers:
    *   Development Status :: 5 - Production/Stable
    *   License :: OSI Approved :: GNU Lesser General Public License v3
    *   Operating System :: OS Independent
    *   Programming Language :: Python

This project comprises

*   `sources` – a directory containing the package sources,

*   `project` – a file containing a script to manage the project, and

*   `README.md` – a file containing a description of the project.

`project`
=========

The command

    project usage

writes a short usage message about `project` to the standard output.

The command

    project help

writes a longer help message about `project` to the standard output.

The command

    project document

writes a description of the project to the file at `README.md` in
[GitLab Flavored Markdown][gfm] format.

The command

    project build

builds in a directory at `objects` a [Python wheel][427] containing a
Python package of the contents of the directory at `sources`, and
copies the wheel to a directory at `targets`.  The version of the
package and the name of the wheel are derived from the Git repository of the
project (see below).

The command

    project clean

deletes directories at `objects` and `targets`.

Package Version and Wheel Name
------------------------------

If the most recent of the Git commits among the first ancestors of the current
`HEAD` commit with an annotated tag of the form

    version/«prefix»[-«suffix»]

(where `«prefix»` does not contain the character `-`) is `«count»`
commits prior to the current `HEAD` commit then the command

    project build

first assigns the dunder `__version__` of each file `__init__.py` of the
package the version

    «prefix»[0-«suffix»].«count»+«metadata»

where

*   `«commit»` is the first seven characters of the ID of the current
    `HEAD` commit,

*   `«metadata»` is `«commit»` if the Git working tree matches the current
    `HEAD` commit, and

*   `«metadata»` is `«commit».dirty` if the Git working tree does not match
    the current `HEAD` commit;

and then creates a wheel with the name

    «name»-«version»-py3-none-any.whl

where

*   `«name»` is the package name, and

*   `«version»` is the [Python normalisation][440] of the package version.

For example, suppose that

*   the first seven characters of the ID of the current `HEAD` commit are
    `4af9f46`, and

*   the package name is `bedrock`.

If

*   the first grandparent (i.e., the first parent of the first parent) of the
    current `HEAD` commit is given the annotated tag `version/1.7`, and

*   the Git working tree matches the current `HEAD` commit

then

*   the package version is `1.7.2+4af9f46`, and

*   the wheel name is `bedrock-1.7.2+4af9f46-py3-none-any.whl`;

if

*   the first parent of the current `HEAD` commit is given the annotated tag
    `version/2.0-rc`, and

*   the Git working tree still matches the current `HEAD` commit

then

*   the package version is `2.0.0-rc.1+4af9f46`, and

*   the wheel name is `bedrock-2.0.0rc1+4af9f46-py3-none-any.whl`; and

if

*   the current `HEAD` commit is given the annotated tag `version/2.0`, but

*   the Git working tree no longer matches the current `HEAD` commit

then

*   the package version is `2.0.0+4af9f46.dirty`, and

*   the wheel name is `bedrock-2.0.0+4af9f46.dirty-py3-none-any.whl`.

References
----------

["GitLab Flavored Markdown"][gfm]
["PEP 427 -- The Wheel Binary Package Format 1.0"][427]
["PEP 440 -- Version Identification and Dependency Specification"][440]

[gfm]: https://docs.gitlab.com/ee/user/markdown.html
[427]: https://www.python.org/dev/peps/pep-0427/
[440]: https://www.python.org/dev/peps/pep-0440/

Warning
=======

The script `project` generates this file from the project sources, and
overwrites any changes made to it.  In order to permanently change this file,
edit the project sources and call the command `project document`.
