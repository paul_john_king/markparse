# coding: utf-8

# markparse - A command-line parser that outputs help in a Markdown format
# Copyright (c) 2020  Paul John King (p.king@openinfrastructure.de)
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License v3.0 as published by the
# Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from argparse import(
	REMAINDER, SUPPRESS,
	Action, ArgumentParser,
	_CountAction, _SubParsersAction
)


class MarkdownArgumentParser(ArgumentParser):

	_item_rule = 76 * "-"

	@staticmethod
	def _codify(text):
		return "`%s`"%(text)

	@staticmethod
	def _item(text):
		if text:
			lines = str(text).splitlines()
			lines[0] = "*   %s"%(lines[0])
			lines[1:] = [
				"    %s"%(line)
				for line
				in lines[1:]
			]
			return "\n".join(lines)
		else:
			return ""

	@staticmethod
	def _metavar(action):
		if action.nargs == "*" or action.nargs == "+":
			return "%(metavar)s [%(metavar)s …]"%{
				"metavar": action.metavar
			}
		elif action.nargs == REMAINDER:
			return "…"
		else:
			return action.metavar

	def format_usage(self):
		title = "Usage\n-----"

		flagged_arguments = []
		positional_arguments = []
		arguments = []
		for action in self._actions:
			if action.metavar:
				metavar = self._metavar(action)
			elif action.choices:
				metavar = "|".join([str(choice) for choice in action.choices])
				if isinstance(action, _SubParsersAction):
					metavar = "%s …"%(metavar)
			else:
				metavar = None

			if action.option_strings:
				option_string = action.option_strings[0]
			else:
				option_string = None

			argument = " ".join(
				[
					str(component)
					for component
					in [option_string, metavar]
					if component
				]
			)
			if isinstance(action, _CountAction):
				argument = "%(argument)s [%(argument)s …]"%{
					"argument": argument
				}
			if not isinstance(action, _SubParsersAction) and not action.required:
				argument = "[%(argument)s]"%{
					"argument": argument
				}
			if action.option_strings:
				flagged_arguments.append(argument)
			else:
				positional_arguments.append(argument)
		arguments = flagged_arguments + positional_arguments

		if self.prog:
			invocation = "    %(prog)s"%{
				"prog": self.prog
			}
			length = len(invocation)
			for argument in arguments:
				if length + 1 + len(argument) <= 80:
					invocation = "%(invocation)s %(argument)s"%{
						"invocation": invocation,
						"argument": argument
					}
					length = length + 1 + len(argument)
				else:
					invocation = "%(invocation)s\n        %(argument)s"%{
						"invocation": invocation,
						"argument": argument
					}
					length = 8 + len(argument)
		else:
			invocation = None

		if self.description:
			description = self.description.strip()%{
				"prog": self.prog
			}
		else:
			description = None

		usage = "\n\n".join(
			component
			for component
			in [title, invocation, description]
			if component
		)

		return "%s\n"%(usage)

	def format_help(self):
		usage = self.format_usage().strip()
		commands = None
		arguments = None
		epilog = None

		flagged_items = []
		positional_items = []
		items = []
		for action in self._actions:
			if isinstance(action, _SubParsersAction):
				title = "Commands\n--------"

				description = action.help

				items = [
					self._item(
						"%(argument)s\n\n%(description)s"%{
							"argument": self._codify(action.dest),
							"description": action.help.strip()
						}
					)
					for action
					in action._choices_actions
				]

				commands = "\n\n".join(
					component
					for component
					in [title, description] + items
					if component
				)
			else:
				if action.metavar:
					metavar = self._metavar(action)
				elif action.choices:
					metavar = "|".join([str(choice) for choice in action.choices])
				else:
					metavar = None

				if action.option_strings:
					arguments = ", ".join(
						[
							self._codify(
								" ".join(
									str(component)
									for component
									in [option_string, metavar]
									if component
								)
							)
							for option_string
							in action.option_strings
						]
					)
				else:
					arguments = self._codify(action.metavar)

				help = action.help

				coda_components = []
				coda_components.append(self._item_rule)
				coda_components.append(
					"*   Required: %(required)s"%{
						"required": action.required
					}
				)
				if action.choices:
					coda_components.append(
						"*   Choices: %(choices)s"%{
							"choices": "|".join(
								[
									self._codify(choice)
									for choice
									in action.choices
								]
							)
						}
					)
				if action.default and action.default != SUPPRESS:
					coda_components.append(
						"*   Default: %(default)s"%{
							"default": self._codify(action.default)
						}
					)
				coda_components.append(self._item_rule)
				coda = "\n".join(coda_components)

				item = self._item(
					"\n\n".join(
						component
						for component
						in [arguments, help, coda]
						if component
					)
				)
				if action.option_strings:
					flagged_items.append(item)
				else:
					positional_items.append(item)
		items = positional_items + flagged_items

		if items:
			title = "Arguments\n---------"

			arguments = "\n\n".join(
				component
				for component
				in [title] + items
				if component
			)

		if self.epilog:
			epilog = self.epilog%{
				"prog": self.prog
			}

		help = "\n\n".join(
			component
			for component
			in [usage, commands, arguments, epilog]
			if component
		)

		return "%s\n"%(help)


class UsageAction(Action):

	def __init__(
		self,
		option_strings,
		dest,
		nargs=None,
		const=None,
		default=None,
		type=None,
		choices=None,
		required=False,
		help=None,
		metavar=None
	):
		super(UsageAction, self).__init__(
			option_strings=option_strings,
			dest=dest,
			nargs=0,
			const=const,
			default=default,
			type=type,
			choices=choices,
			required=required,
			help=help,
			metavar=metavar
		)

	def __call__(self, parser, namespace, values, option_string):
		parser.print_usage()
		parser.exit()
