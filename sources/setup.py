# coding: utf-8

# A command-line parser that outputs help in Markdown format
# Copyright (c) 2020  Paul John King (p.king@openinfrastructure.de)
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License v3.0 as published by the
# Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from setuptools import(
	find_packages, setup
)
from warnings import(
	filterwarnings
)

from markparse import(
	__author__, __classifiers__, __doc__, __email__, __license__,
	__package__, __python_requires__, __uri__, __version__
)


if __name__ == "__main__":
	try:
		filterwarnings(
			"ignore",
			message="Normalizing '.*' to '.*'",
			module="setuptools"
		)
		setup(
			name=__package__,
			version=__version__,
			author=__author__,
			author_email=__email__,
			url=__uri__,
			license=__license__,
			description=__doc__.splitlines()[0],
			long_description=__doc__,
			python_requires=__python_requires__,
			classifiers=__classifiers__,
			packages=find_packages()
		)
	except KeyboardInterrupt as exception:
		exit(1)
	except Exception as exception:
		exit(
			"Error: %(cause)s"%{
				"cause": str(exception)
			}
		)
