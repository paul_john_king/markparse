#!/bin/sh -eu
# coding: utf-8

# Utility Functions
# -----------------

_echo_stdin(){
	local _line;

	while IFS="" read -r _line;
	do
		echo "${_line}";
	done;

	return 0;
};

_get_package_name_from_sources_dir(){
	local _name;

	_name=$(echo "${_SOURCES_DIR}"/*/"__init__.py");
	_name="${_name#${_SOURCES_DIR}/}";
	_name="${_name%/__init__.py}";

	echo "${_name}";

	return 0;
};

_get_package_version_from_git_repository(){
	local _PREFIX;

	local _METADATA;
	local _PATCH;
	local _TAG_PREFIX;
	local _TAG_SUFFIX;

	local _version;

	_PREFIX="version/";

	_version=$(
		cd "${_HOME}" &&
		git describe \
			--first-parent \
			--match="${_PREFIX}*" \
			--long \
			--dirty=".dirty";
	);
	_version="${_version#${_PREFIX}}";

	_METADATA="${_version##*-g}";
	_version="${_version%-g${_METADATA}}";

	_PATCH="${_version##*-}";
	_version="${_version%-${_PATCH}}";

	_TAG_PREFIX="${_version%%-*}";
	_version="${_version#${_TAG_PREFIX}}";

	_TAG_SUFFIX="${_version:+.0${_version}}";
	_version="${_TAG_PREFIX}${_TAG_SUFFIX}.${_PATCH}+${_METADATA}";

	echo "${_version}";

	return 0;
};

_underline_texts(){
	local OPTIND;
	local OPTARG;
	local OPTION;

	local _char;
	local _text;
	local _line;

	OPTIND=1;
	_char="=";
	while getopts ":c:" OPTION;
	do
		case "${OPTION}" in
		"c") _char="${OPTARG}";;
		esac;
	done;
	shift $((OPTIND - 1));

	for _text in "${@}";
	do
		_line=""
		while test ${#_line} -lt ${#_text};
		do
			_line=$(printf "%c%s" "${_char}" "${_line}");
		done;
		printf "%s\n%s\n" "${_text}" "${_line}";
	done;

	return 0;
}

# Command Functions
# -----------------

_usage(){
	_echo_stdin << \
────────────────────────────────────────────────────────────────────────────────
Usage:

    ${_SELF} usage
    ${_SELF} help
    ${_SELF} document
    ${_SELF} build
    ${_SELF} clean
────────────────────────────────────────────────────────────────────────────────

	return 0;
};

_help(){
	_echo_stdin << \
────────────────────────────────────────────────────────────────────────────────
The command

    ${_SELF} usage

writes a short usage message about \`${_SELF}\` to the standard output.

The command

    ${_SELF} help

writes a longer help message about \`${_SELF}\` to the standard output.

The command

    ${_SELF} document

writes a description of the project to the file at \`${_README_FILE}\` in
[GitLab Flavored Markdown][gfm] format.

The command

    ${_SELF} build

builds in a directory at \`${_OBJECTS_DIR}\` a [Python wheel][427] containing a
Python package of the contents of the directory at \`${_SOURCES_DIR}\`, and
copies the wheel to a directory at \`${_TARGETS_DIR}\`.  The version of the
package and the name of the wheel are derived from the Git repository of the
project (see below).

The command

    ${_SELF} clean

deletes directories at \`${_OBJECTS_DIR}\` and \`${_TARGETS_DIR}\`.

$(_underline_texts -c "-" "Package Version and Wheel Name")

If the most recent of the Git commits among the first ancestors of the current
\`HEAD\` commit with an annotated tag of the form

    version/«prefix»[-«suffix»]

(where \`«prefix»\` does not contain the character \`-\`) is \`«count»\`
commits prior to the current \`HEAD\` commit then the command

    ${_SELF} build

first assigns the dunder \`__version__\` of each file \`__init__.py\` of the
package the version

    «prefix»[0-«suffix»].«count»+«metadata»

where

*   \`«commit»\` is the first seven characters of the ID of the current
    \`HEAD\` commit,

*   \`«metadata»\` is \`«commit»\` if the Git working tree matches the current
    \`HEAD\` commit, and

*   \`«metadata»\` is \`«commit».dirty\` if the Git working tree does not match
    the current \`HEAD\` commit;

and then creates a wheel with the name

    «name»-«version»-py3-none-any.whl

where

*   \`«name»\` is the package name, and

*   \`«version»\` is the [Python normalisation][440] of the package version.

For example, suppose that

*   the first seven characters of the ID of the current \`HEAD\` commit are
    \`4af9f46\`, and

*   the package name is \`bedrock\`.

If

*   the first grandparent (i.e., the first parent of the first parent) of the
    current \`HEAD\` commit is given the annotated tag \`version/1.7\`, and

*   the Git working tree matches the current \`HEAD\` commit

then

*   the package version is \`1.7.2+4af9f46\`, and

*   the wheel name is \`bedrock-1.7.2+4af9f46-py3-none-any.whl\`;

if

*   the first parent of the current \`HEAD\` commit is given the annotated tag
    \`version/2.0-rc\`, and

*   the Git working tree still matches the current \`HEAD\` commit

then

*   the package version is \`2.0.0-rc.1+4af9f46\`, and

*   the wheel name is \`bedrock-2.0.0rc1+4af9f46-py3-none-any.whl\`; and

if

*   the current \`HEAD\` commit is given the annotated tag \`version/2.0\`, but

*   the Git working tree no longer matches the current \`HEAD\` commit

then

*   the package version is \`2.0.0+4af9f46.dirty\`, and

*   the wheel name is \`bedrock-2.0.0+4af9f46.dirty-py3-none-any.whl\`.

$(_underline_texts -c "-" "References")

["GitLab Flavored Markdown"][gfm]
["PEP 427 -- The Wheel Binary Package Format 1.0"][427]
["PEP 440 -- Version Identification and Dependency Specification"][440]

[gfm]: https://docs.gitlab.com/ee/user/markdown.html
[427]: https://www.python.org/dev/peps/pep-0427/
[440]: https://www.python.org/dev/peps/pep-0440/
────────────────────────────────────────────────────────────────────────────────

	return 0;
};

_document(){
	local _WARNING="\
The script \`${_SELF}\` generates this file from the project sources, and
overwrites any changes made to it.  In order to permanently change this file,
edit the project sources and call the command \`${_SELF} document\`.";

	_echo_stdin > "${_README_PATH}" << \
————————————————————————————————————————————————————————————————————————————————
<!--
${_WARNING}
-->

$(
    cd "${_SOURCES_PATH}" &&
    python3 -B -m ${_PACKAGE_NAME}
)

This project comprises

*   \`${_SOURCES_DIR}\` – a directory containing the package sources,

*   \`${_SELF}\` – a file containing a script to manage the project, and

*   \`${_README_FILE}\` – a file containing a description of the project.

$(_underline_texts "\`${_SELF}\`")

$("${_HOME}/${_SELF}" "help")

$(_underline_texts "Warning")

${_WARNING}
————————————————————————————————————————————————————————————————————————————————

	return 0;
};

_build(){
	local _source_path;
	local _object_path;
	local _line;

	rm -rf "${_OBJECTS_PATH}";
	cp -a "${_SOURCES_PATH}" "${_OBJECTS_PATH}";
	mkdir -p "${_TARGETS_PATH}";

	find "${_SOURCES_PATH}" -type f -name "__init__.py" |
	while IFS="" read -r _source_path;
	do
		_object_path="${_OBJECTS_PATH}${_source_path#${_SOURCES_PATH}}";
		while IFS="" read -r _line; do
			case "${_line}" in
				("__version__ = "*) printf "__version__ = \"%s\"\n" "${_PACKAGE_VERSION}";;
				(*) printf "%s\n" "${_line}";;
			esac;
		done < "${_source_path}" > "${_object_path}";
	done;

	(
		cd "${_OBJECTS_PATH}" &&
		python3 -B "setup.py" "bdist_wheel";
	)

	cp -a "${_OBJECTS_PATH}/dist"/* "${_TARGETS_DIR}";

	return 0;
};

_clean(){
	rm -rf "${_OBJECTS_PATH}" "${_TARGETS_PATH}";

	return 0;
};

_fail(){
	_echo_stdin >&2 << \
────────────────────────────────────────────────────────────────────────────────
Error: ${@}
$(_usage)
────────────────────────────────────────────────────────────────────────────────

	return 1;
};

# The Main Function
# -----------------

_main(){
	local _HOME;
	local _SELF;

	local _SOURCES_DIR;
	local _OBJECTS_DIR;
	local _TARGETS_DIR;
	local _README_FILE;

	local _SOURCES_PATH;
	local _OBJECTS_PATH;
	local _TARGETS_PATH;
	local _README_PATH;

	local _PACKAGE_NAME;
	local _PACKAGE_VERSION;

	local _COMMAND;

	_HOME="${0%/*}";
	_SELF="${0##*/}";

	_SOURCES_DIR="sources";
	_OBJECTS_DIR="objects";
	_TARGETS_DIR="targets";
	_README_FILE="README.md";

	_SOURCES_PATH="${_HOME}/${_SOURCES_DIR}";
	_OBJECTS_PATH="${_HOME}/${_OBJECTS_DIR}";
	_TARGETS_PATH="${_HOME}/${_TARGETS_DIR}";
	_README_PATH="${_HOME}/${_README_FILE}";

	_PACKAGE_NAME=$(_get_package_name_from_sources_dir);
	_PACKAGE_VERSION=$(_get_package_version_from_git_repository);

	if test ${#} -ge 1;
	then
		_COMMAND="${1}";
		shift 1;
		case "${_COMMAND}" in
			("usage") _usage "${@}";;
			("help") _help "${@}";;
			("document") _document "${@}";;
			("build") _build "${@}";;
			("clean") _clean "${@}";;
			(*) _fail "\`${_SELF}\` has no command \`${_COMMAND}\`.";;
		esac;
	else
		_usage;
	fi;

	return 0;
};

_main "${@}";
